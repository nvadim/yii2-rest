<?php

namespace app\models;

use yii;

/**
 * This is the model class for table "ru1cbit4_test.b_user".
 *
 * @property integer $ID
 * @property string $TIMESTAMP_X
 * @property string $LOGIN
 * @property string $PASSWORD
 * @property string $CHECKWORD
 * @property string $ACTIVE
 * @property string $NAME
 * @property string $LAST_NAME
 * @property string $EMAIL
 * @property string $LAST_LOGIN
 * @property string $DATE_REGISTER
 * @property string $LID
 * @property string $PERSONAL_PROFESSION
 * @property string $PERSONAL_WWW
 * @property string $PERSONAL_ICQ
 * @property string $PERSONAL_GENDER
 * @property string $PERSONAL_BIRTHDATE
 * @property integer $PERSONAL_PHOTO
 * @property string $PERSONAL_PHONE
 * @property string $PERSONAL_FAX
 * @property string $PERSONAL_MOBILE
 * @property string $PERSONAL_PAGER
 * @property string $PERSONAL_STREET
 * @property string $PERSONAL_MAILBOX
 * @property string $PERSONAL_CITY
 * @property string $PERSONAL_STATE
 * @property string $PERSONAL_ZIP
 * @property string $PERSONAL_COUNTRY
 * @property string $PERSONAL_NOTES
 * @property string $WORK_COMPANY
 * @property string $WORK_DEPARTMENT
 * @property string $WORK_POSITION
 * @property string $WORK_WWW
 * @property string $WORK_PHONE
 * @property string $WORK_FAX
 * @property string $WORK_PAGER
 * @property string $WORK_STREET
 * @property string $WORK_MAILBOX
 * @property string $WORK_CITY
 * @property string $WORK_STATE
 * @property string $WORK_ZIP
 * @property string $WORK_COUNTRY
 * @property string $WORK_PROFILE
 * @property integer $WORK_LOGO
 * @property string $WORK_NOTES
 * @property string $ADMIN_NOTES
 * @property string $STORED_HASH
 * @property string $XML_ID
 * @property string $PERSONAL_BIRTHDAY
 * @property string $EXTERNAL_AUTH_ID
 * @property string $CHECKWORD_TIME
 * @property string $SECOND_NAME
 * @property string $CONFIRM_CODE
 * @property integer $LOGIN_ATTEMPTS
 * @property string $LAST_ACTIVITY_DATE
 * @property string $AUTO_TIME_ZONE
 * @property string $TIME_ZONE
 * @property integer $TIME_ZONE_OFFSET
 * @property string $TITLE
 */
class BxUser extends yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'b_user';
    }


//    public static function primaryKey()
//    {
//        return ['XML_ID'];
//    }

    public static function findIdentity($id)
    {
        return static::findOne(['ID' => $id]);
    }

    public static function findByXmlId($XML_ID)
    {
        return static::findOne(['XML_ID' => $XML_ID]);
    }

//    public static function findIdentity($id)
//    {
//        return static::findOne($id);
//    }
//    public static function findIdentityByAccessToken($token, $type = null)
//    {
//        return static::findOne(['access_token' => $token]);
//    }
//    public function getAuthKey()
//    {
//        return $this->authKey;
//    }
//    public function validateAuthKey($authKey)
//    {
//        return $this->authKey === $authKey;
//    }

    public function fields()
    {

        return [
            'XML_ID',
            'LAST_NAME',
            'NAME',

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['TIMESTAMP_X', 'LAST_LOGIN', 'DATE_REGISTER', 'PERSONAL_BIRTHDAY', 'CHECKWORD_TIME', 'LAST_ACTIVITY_DATE'], 'safe'],
            [['LOGIN', 'PASSWORD', 'DATE_REGISTER'], 'required'],
            [['PERSONAL_PHOTO', 'WORK_LOGO', 'LOGIN_ATTEMPTS', 'TIME_ZONE_OFFSET'], 'integer'],
            [['PERSONAL_STREET', 'PERSONAL_NOTES', 'WORK_STREET', 'WORK_PROFILE', 'WORK_NOTES', 'ADMIN_NOTES'], 'string'],
            [['LOGIN', 'PASSWORD', 'CHECKWORD', 'NAME', 'LAST_NAME', 'PERSONAL_BIRTHDATE', 'SECOND_NAME', 'TIME_ZONE'], 'string', 'max' => 50],
            [['ACTIVE', 'PERSONAL_GENDER', 'AUTO_TIME_ZONE'], 'string', 'max' => 1],
            [['EMAIL', 'PERSONAL_PROFESSION', 'PERSONAL_WWW', 'PERSONAL_ICQ', 'PERSONAL_PHONE', 'PERSONAL_FAX', 'PERSONAL_MOBILE', 'PERSONAL_PAGER', 'PERSONAL_MAILBOX', 'PERSONAL_CITY', 'PERSONAL_STATE', 'PERSONAL_ZIP', 'PERSONAL_COUNTRY', 'WORK_COMPANY', 'WORK_DEPARTMENT', 'WORK_POSITION', 'WORK_WWW', 'WORK_PHONE', 'WORK_FAX', 'WORK_PAGER', 'WORK_MAILBOX', 'WORK_CITY', 'WORK_STATE', 'WORK_ZIP', 'WORK_COUNTRY', 'XML_ID', 'EXTERNAL_AUTH_ID', 'TITLE'], 'string', 'max' => 255],
            [['LID'], 'string', 'max' => 2],
            [['STORED_HASH'], 'string', 'max' => 32],
            [['CONFIRM_CODE'], 'string', 'max' => 8],
            [['LOGIN', 'EXTERNAL_AUTH_ID'], 'unique', 'targetAttribute' => ['LOGIN', 'EXTERNAL_AUTH_ID'], 'message' => 'The combination of Login and External  Auth  ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ID' => 'ID',
            'TIMESTAMP_X' => 'Timestamp  X',
            'LOGIN' => 'Login',
            'PASSWORD' => 'Password',
            'CHECKWORD' => 'Checkword',
            'ACTIVE' => 'Active',
            'NAME' => 'Name',
            'LAST_NAME' => 'Last  Name',
            'EMAIL' => 'Email',
            'LAST_LOGIN' => 'Last  Login',
            'DATE_REGISTER' => 'Date  Register',
            'LID' => 'Lid',
            'PERSONAL_PROFESSION' => 'Personal  Profession',
            'PERSONAL_WWW' => 'Personal  Www',
            'PERSONAL_ICQ' => 'Personal  Icq',
            'PERSONAL_GENDER' => 'Personal  Gender',
            'PERSONAL_BIRTHDATE' => 'Personal  Birthdate',
            'PERSONAL_PHOTO' => 'Personal  Photo',
            'PERSONAL_PHONE' => 'Personal  Phone',
            'PERSONAL_FAX' => 'Personal  Fax',
            'PERSONAL_MOBILE' => 'Personal  Mobile',
            'PERSONAL_PAGER' => 'Personal  Pager',
            'PERSONAL_STREET' => 'Personal  Street',
            'PERSONAL_MAILBOX' => 'Personal  Mailbox',
            'PERSONAL_CITY' => 'Personal  City',
            'PERSONAL_STATE' => 'Personal  State',
            'PERSONAL_ZIP' => 'Personal  Zip',
            'PERSONAL_COUNTRY' => 'Personal  Country',
            'PERSONAL_NOTES' => 'Personal  Notes',
            'WORK_COMPANY' => 'Work  Company',
            'WORK_DEPARTMENT' => 'Work  Department',
            'WORK_POSITION' => 'Work  Position',
            'WORK_WWW' => 'Work  Www',
            'WORK_PHONE' => 'Work  Phone',
            'WORK_FAX' => 'Work  Fax',
            'WORK_PAGER' => 'Work  Pager',
            'WORK_STREET' => 'Work  Street',
            'WORK_MAILBOX' => 'Work  Mailbox',
            'WORK_CITY' => 'Work  City',
            'WORK_STATE' => 'Work  State',
            'WORK_ZIP' => 'Work  Zip',
            'WORK_COUNTRY' => 'Work  Country',
            'WORK_PROFILE' => 'Work  Profile',
            'WORK_LOGO' => 'Work  Logo',
            'WORK_NOTES' => 'Work  Notes',
            'ADMIN_NOTES' => 'Admin  Notes',
            'STORED_HASH' => 'Stored  Hash',
            'XML_ID' => 'Xml  ID',
            'PERSONAL_BIRTHDAY' => 'Personal  Birthday',
            'EXTERNAL_AUTH_ID' => 'External  Auth  ID',
            'CHECKWORD_TIME' => 'Checkword  Time',
            'SECOND_NAME' => 'Second  Name',
            'CONFIRM_CODE' => 'Confirm  Code',
            'LOGIN_ATTEMPTS' => 'Login  Attempts',
            'LAST_ACTIVITY_DATE' => 'Last  Activity  Date',
            'AUTO_TIME_ZONE' => 'Auto  Time  Zone',
            'TIME_ZONE' => 'Time  Zone',
            'TIME_ZONE_OFFSET' => 'Time  Zone  Offset',
            'TITLE' => 'Title',
        ];
    }
}
